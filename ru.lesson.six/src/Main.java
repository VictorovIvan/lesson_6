import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        Task task = new Task();
        String filePath = new File("resources","WarAndPiece.txt").getAbsolutePath();
        try {
            System.out.println("Количество слов (Страдание) в задаче №1: ".concat(task.getCountIn(filePath).toString()));
            System.out.println("\nКоличество слов (Страдание) в задаче №2: ".concat(task.getCountInScanner(filePath).toString()));
            System.out.println("\nВывод в консоль таблицы умножения (Задача 3):");task.tableMultiply();
        } catch (IOException e) {
            System.out.println("Отсутствует файл");
        }

    }
}
