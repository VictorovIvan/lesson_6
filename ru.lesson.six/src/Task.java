import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task {


    public Integer getCountIn(String path) throws IOException {
        Integer cnt = 0;
        Path file = Paths.get((path));
        List<String> lines = Files.readAllLines(file);
        for (String line : lines) {
            Pattern pattern = Pattern.compile(".страдани*.", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(line);
            while (matcher.find()) {
                cnt++;
            }
        }
        return cnt;
    }

    public Integer getCountInScanner(String path) throws IOException {
        Integer cnt = 0;
        Scanner sc = new Scanner(new FileReader((path)));
        while ((sc.hasNextLine())) {
            String line = sc.nextLine();
            Pattern pattern = Pattern.compile(".страдани*.", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(line);
            while (matcher.find()) {
                cnt++;
            }
        }
        return cnt;
    }

    public void tableMultiply() {
        int[] mulTable = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int lines : mulTable) {
            for (int number : mulTable) {
                System.out.printf("%4d", (number * lines));
            }
            System.out.println();
        }
    }
}
